/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_UTILS__
#define __CALIMIRO_UTILS__

#include <filesystem>
#include <functional>
#include <map>
#include <string>
#include <unistd.h> // for getpid()
#include <vector>

#include <glm/glm.hpp>

#include "./calimiro/abstract_logger.h"


namespace calimiro
{
namespace utils
{

/**
 * special inserter that has a behavior similar to python's groupby.
 *    Usage:
 *
 *    struct pt
 *    {
 *    float x;
 *    float y;
 *	};
 *
 *    std::vector<pt> pts;
 *    pts.push_back({ 1.0, 1.5 });
 *    pts.push_back({ 1.5, 7.8 });
 *    pts.push_back({ 1.0, 5.2 });
 *    pts.push_back({ 2, 3.2 });
 *
 *    std::sort(pts.begin(), pts.end(), [](const pts & lhs, const pts & rhs) {
 *            return lhs.x < rhs.x;
 *        });
 *
 *    std::map<float, std::vector<pt>> pts_by_x;
 *    std::for_each(pts.begin(), pts.end(), inserter<float, pt>(pts_by_x, [](pt a){ return a.x;}));
 *
 *    for (const auto& group : pts_by_x)
 *    {
 *        std::cout << "\t" << group.first << "\n";
 *        for (const auto& point : group.second)
 *        {
 *            std::cout << point.x << " " << point.y << std::endl;
 *        }
 *    }
 */
template <typename T, typename U>
class inserter
{
  public:
    using map_type = std::map<T, std::vector<U>>;

    inserter(map_type& m, std::function<T(U)> f)
        : _map(m)
        , _select_func(f)
        , _it(_map.end())
    {
    }

    void operator()(const U& obj)
    {
        auto key_val = _select_func(obj);
        if (_it != _map.end() && _last_elt == key_val)
        {
            _it->second.push_back(obj);
        }
        else
        {
            _it = _map.insert(make_pair(key_val, std::vector<U>())).first;
            _it->second.push_back(obj);
            _last_elt = key_val;
        }
    }

  private:
    map_type& _map;
    std::function<T(U)> _select_func;
    typename map_type::iterator _it;
    T _last_elt;
};

/**
 * \brief read a ply file (only the vertice coordinates)
 * \param filename name of the Ply file to read
 * \return Return vector<glm::vec3>
 */
std::vector<glm::vec3> readVerticesFromPly(AbstractLogger* log, const std::string& filename);

/**
 * \brief saves a vector<glm::vec3> to file following the Ply format
 * \param pts vector<glm::vec3> to be saved
 * \param filename name of file under which vertices will be saved
 * \return Return false if could not write pts to file
 */
bool writeVerticesToPly(AbstractLogger* log, const std::string& filename, const std::vector<glm::vec3> vertices);

/**
 * \brief compute the mean of a vector; source: https://stackoverflow.com/questions/7616511/calculate-mean-and-standard-deviation-from-a-vector-of-samples-in-c-using-boos
 * \param v vector of double
 * \return Return the mean of vector v
 */

double computeMean(const std::vector<double> v);

/**
 * \brief compute the means of a vector of glm::vec3 or reference_wrapper<glm::vec3>
 * \param vectors list of pointers to vectors
 * \return Return a glm::vec3 of means
 */
template <class V>
glm::vec3 computeMeanOfVectors(const std::vector<V>& vectors)
{
    glm::vec3 mean(0.f);
    for (const auto& vector : vectors) {
        mean += static_cast<glm::vec3>(vector);
    }

    return mean / static_cast<float>(vectors.size());
}

/**
 * \brief compute the standard deviation of a vector
 * \param v vector
 * \param m mean of v
 * \return Return standard deviation
 */
double computeStandardDeviation(const std::vector<double> v, double m);

/**
 * \brief Cholesky decomposition is the decomposition of positive-definite matrix A such that A = LU. L being a lower triangular matrix, U an upper triangular matrix where U =
 * L_transpose
 * \param A is a 3x3 matrix
 * \return Return x a lower triangular matrix
 */
glm::mat3 cholesky3x3(const glm::mat3 A);

/**
 * \brief Solve a linear system of equations of the form U*x = B (in dimension 3)
 * \param U is an 3x3 upper triangular matrix
 * \param B 3D coordinate vector
 * \return Return x a 3D coordinate vector
 */
glm::vec3 backwardSubstitution(const glm::mat3 U, const glm::vec3 B);

/**
 * \brief Solve a linear system of equations of the form L*x = B (in dimension 3)
 * \param A is a 3x3 lower triangular matrix
 * \param B 3D coordinate vector
 * \return Return x a 3D coordinate vector
 */
glm::vec3 forwardSubstitution(const glm::mat3 L, const glm::vec3 B);

/**
 * \brief Return the workspace path
 */
inline std::filesystem::path getWorkspace()
{
    return "/tmp/calimiro_" + std::to_string(getpid());
};

/**
 * \brief Return the number of files in a directory
 */
inline std::size_t nbFilesInDirectory(std::filesystem::path path)
{
    return std::distance(std::filesystem::directory_iterator(path), {});
};

/**
 * \brief Saves the metadata of an image to file
 */
bool writeMetadata(const std::filesystem::path& filename);

template <typename T>
std::vector<T> extractElts(const std::vector<T>& xs, const std::vector<int>& sample)
{
    std::vector<T> sampled_xs;
    for (size_t i = 0; i < sample.size(); ++i)
    {
        sampled_xs.push_back(xs[sample[i]]);
    }
    return sampled_xs;
}
}; // namespace utils
}; // namespace calimiro

#endif // __CALIMIRO_UTILS__
