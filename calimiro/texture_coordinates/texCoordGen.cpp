/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "./calimiro/texture_coordinates/texCoordGen.h"

#include <glm/gtx/transform.hpp>

namespace calimiro
{

glm::mat4 TexCoordGen::getUVtransformationMatrix() const
{
    const glm::vec3 desiredAlignmentForRotation(0.f, 0.f, -1.f); // unit vector
    glm::mat4 transformation_matrix;

    // Rotation
    const float dotP = glm::dot(_eye_direction, desiredAlignmentForRotation);
    if (1 - dotP < FLT_EPSILON) // dot(a, a) = 1
    {
        // CrossP = 0 && Vectors aligned
        // Solution: no rotation
        transformation_matrix = glm::mat4(1.f); // Identity matrix
    }
    else if (1 + dotP < FLT_EPSILON) // dot(a, -a) = -1
    {
        // CrossP = 0 && Vectors opposed
        // Solution: rotate 180deg(PI rad) around the y axis
        transformation_matrix = glm::mat4(-1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, -1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
    }
    else
    {
        // General case
        const auto angle = std::acos(dotP); // using unit vectors, division by 1
        const glm::vec3 axis = glm::cross(_eye_direction, desiredAlignmentForRotation);
        transformation_matrix = glm::rotate(angle, axis);
    }

    // Translation
    transformation_matrix[0][3] = -_eye_position.x;
    transformation_matrix[1][3] = -_eye_position.y;
    transformation_matrix[2][3] = -_eye_position.z;

    return transformation_matrix;
}

glm::mat3 TexCoordGen::getHorizonBiasMatrix() const
{
    const auto rad = _horizon_rotation * constants::deg_to_rad;
    return glm::mat3(std::cos(rad), -std::sin(rad), 0.f, std::sin(rad), std::cos(rad), 0.f, 0.f, 0.f, 1.f);
}

glm::mat3 TexCoordGen::getFlipMatrix() const
{
    return glm::mat3(_flip_horizontal ? -1.f : 1.f, 0.f, 0.f, 0.f, _flip_vertical ? -1.f : 1.f, 0.f, 0.f, 0.f, 1.f);
}

void TexCoordGen::uvUnitSquareRemap(std::vector<glm::vec2>& uvs) const
{
    // Find the min and max values on both dimensions (x, y)
    float minu, maxu;
    float minv, maxv;

    minu = minv = std::numeric_limits<float>::max();
    maxu = maxv = -std::numeric_limits<float>::max();

    for (const auto& uv : uvs)
    {
        minu = uv.x < minu ? uv.x : minu;
        minv = uv.y < minv ? uv.y : minv;
        maxu = uv.x > maxu ? uv.x : maxu;
        maxv = uv.y > maxv ? uv.y : maxv;
    }

    // Move bottom-left to origin, it makes the next step simpler and the pivot accurate (i.e. no scaling as a side-effect)
    for (auto& uv : uvs)
    {
        uv.x -= minu;
        uv.y -= minv;
    }
    maxu = maxu - minu;
    maxv = maxv - minv;
    minu = minv = 0.f;

    // Keep object ratio
    float min, max;

    if (maxu > maxv)
    {
        const auto ratio = maxv / maxu;
        for (auto& uv : uvs)
            uv = glm::vec2(uv.x, ratio * uv.y);
        min = minu;
        max = maxu;
    }
    else
    {
        const auto ratio = maxu / maxv;
        for (auto& uv : uvs)
            uv = glm::vec2(ratio * uv.x, uv.y);
        min = minv;
        max = maxv;
    }

    // Remap each vector from inside the box (min, max) to a 1x1 square so it can be used in a uv map
    for (auto& uv : uvs)
    {
        uv.x = remap(uv.x, min, max, 0.f, 1.f);
        uv.y = remap(uv.y, min, max, 0.f, 1.f);
    }
}

} // namespace calimiro
