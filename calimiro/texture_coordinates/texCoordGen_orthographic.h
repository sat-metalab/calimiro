/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_TEXTURE_COORDINATES_GENERATOR_ORTHOGRAPHIC__
#define __CALIMIRO_TEXTURE_COORDINATES_GENERATOR_ORTHOGRAPHIC__

#include "./calimiro/texture_coordinates/texCoordGen.h"

namespace calimiro
{

class TexCoordGenOrthographic : public TexCoordGen
{
  public:
    TexCoordGenOrthographic(AbstractLogger* log,
        const std::vector<glm::vec3>& vertices,
        const glm::vec3& eye_position,
        const glm::vec3& eye_direction,
        const float horizon_rotation,
        const bool flip_horizontal,
        const bool flip_vertical)
        : TexCoordGen(log, vertices, eye_position, eye_direction, horizon_rotation, flip_horizontal, flip_vertical){};

    std::vector<glm::vec2> computeTextureCoordinates() override final;
};

} // namespace calimiro

#endif // __CALIMIRO_TEXTURE_COORDINATES_GENERATOR_ORTHOGRAPHIC__
