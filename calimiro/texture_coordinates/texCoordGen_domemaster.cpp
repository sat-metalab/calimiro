/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/texture_coordinates/texCoordGen_domemaster.h"

namespace calimiro
{

std::vector<glm::vec2> TexCoordGenDomeMaster::computeTextureCoordinates()
{
    _log->message("Generating dome master texture coordinates with the following parameters:"
                  "\n  Eye position (%, %, %)"
                  "\n  Eye direction (%, %, %)"
                  "\n  Horizon rotation: %deg"
                  "\n  Flip horizontal: %"
                  "\n  Flip vertical: %"
                  "\n  Field of view: %deg",
        std::to_string(_eye_position.x),
        std::to_string(_eye_position.y),
        std::to_string(_eye_position.z),
        std::to_string(_eye_direction.x),
        std::to_string(_eye_direction.y),
        std::to_string(_eye_direction.z),
        std::to_string(_horizon_rotation),
        (_flip_horizontal ? "TRUE" : "FALSE"),
        (_flip_vertical ? "TRUE" : "FALSE"),
        std::to_string(_fov));

    std::vector<glm::vec2> new_uvs;

    const auto transformation_matrix_3d = getUVtransformationMatrix();
    const auto transformation_matrix_2d = getFlipMatrix() * getHorizonBiasMatrix();

    const auto half_fov = 1.f / (0.5f * _fov);
    const auto origin_for_latitude = glm::vec3(0.f, 0.f, -1.f);

    for (const auto& vertex : _vertices)
    {
        const auto point = glm::vec4(vertex, 1.f) * transformation_matrix_3d;
        const auto unit = glm::normalize(glm::vec3(point));

        const auto latitude = std::acos(dot(unit, origin_for_latitude)) * constants::rad_to_deg * half_fov;

        const auto longitude_point = glm::vec2(point.x, point.y);
        const auto longitude_point_unit = glm::length(longitude_point) < FLT_EPSILON ? glm::vec2(0.f, 0.f) : glm::normalize(longitude_point);

        const auto uv_homogeneous = glm::vec3(latitude * longitude_point_unit.x, latitude * longitude_point_unit.y, 1.f) * transformation_matrix_2d;
        const auto uv = glm::vec2(std::fma(uv_homogeneous.x, 0.5f, 0.5f), std::fma(uv_homogeneous.y, 0.5f, 0.5f));
        new_uvs.push_back(uv);
    }

    return new_uvs;
};

} // namespace calimiro
