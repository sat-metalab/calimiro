/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./calimiro/texture_coordinates/texCoordGen_orthographic.h"

namespace calimiro
{

std::vector<glm::vec2> TexCoordGenOrthographic::computeTextureCoordinates()
{
    _log->message("Generating orthographic texture coordinates with the following parameters:"
                  "\n  Eye position (%, %, %)"
                  "\n  Eye direction (%, %, %)"
                  "\n  Horizon rotation: %deg"
                  "\n  Flip horizontal: %"
                  "\n  Flip vertical: %",
        std::to_string(_eye_position.x),
        std::to_string(_eye_position.y),
        std::to_string(_eye_position.z),
        std::to_string(_eye_direction.x),
        std::to_string(_eye_direction.y),
        std::to_string(_eye_direction.z),
        std::to_string(_horizon_rotation),
        (_flip_horizontal ? "TRUE" : "FALSE"),
        (_flip_vertical ? "TRUE" : "FALSE"));

    std::vector<glm::vec2> new_uvs;

    const auto transformation_matrix_3d = getUVtransformationMatrix();
    const auto transformation_matrix_2d = getFlipMatrix() * getHorizonBiasMatrix();

    for (const auto& vertex : _vertices)
    {
        const auto vertex_to_plane = (_eye_position + _eye_direction) - vertex;
        const auto l = glm::length(vertex_to_plane);

        const auto angle = M_PI_2f32 - std::acos(glm::clamp(glm::dot(vertex_to_plane, -_eye_direction) / l, -1.0f, 1.0f));

        const auto vertex_plane_normal = l * std::sin(angle);
        const auto point_3d = vertex + (-_eye_direction * vertex_plane_normal);

        const auto point_4d = glm::vec4(point_3d, 1.f) * transformation_matrix_3d;

        const auto uv_homogeneous = glm::vec3(point_4d.x, point_4d.y, 1.f) * transformation_matrix_2d;
        const auto uv = glm::vec2(uv_homogeneous.x, uv_homogeneous.y);
        new_uvs.push_back(uv);
    }

    uvUnitSquareRemap(new_uvs);

    return new_uvs;
};

} // namespace calimiro
