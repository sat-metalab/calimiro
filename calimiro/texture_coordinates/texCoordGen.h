/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_TEXTURE_COORDINATES_GENERATOR__
#define __CALIMIRO_TEXTURE_COORDINATES_GENERATOR__

#include <vector>

#include <glm/glm.hpp>

#include "../abstract_logger.h"
#include "../constants.h"

namespace calimiro
{

class TexCoordGen
{
  public:
    /**
     * Constructor
     * \param log the logger to display messages.
     * \param vertices the mesh's vertices.
     * \param eye_position a vector representing the position of the viewer in 3d space.
     * \param eye_direction a unit vector representing the orientation (direction) of the viewer.
     * \param horizon_rotation rotation in degrees to adjust the horizon
     * \param flip_horizontal flip the texture coordinates map on the horizontal axis
     * \param flip_vertical flip the texture coordinates map on the vertical axis
     */
    TexCoordGen(AbstractLogger* log,
        const std::vector<glm::vec3>& vertices,
        const glm::vec3& eye_position,
        const glm::vec3& eye_direction,
        const float horizon_rotation,
        const bool flip_horizontal,
        const bool flip_vertical)
        : _log(log)
        , _vertices(vertices)
        , _eye_position(eye_position)
        , _eye_direction(eye_direction)
        , _horizon_rotation(horizon_rotation)
        , _flip_horizontal(flip_horizontal)
        , _flip_vertical(flip_vertical){};

    virtual ~TexCoordGen() {}

    /**
     * Compute a set of texture coordinates for a given set of vertices
     * \return a vector of 2d texture coordinates (in glm::vec2 form).
     */
    virtual std::vector<glm::vec2> computeTextureCoordinates() = 0;

  protected:
    AbstractLogger* _log;
    const std::vector<glm::vec3>& _vertices;
    const glm::vec3& _eye_position;
    const glm::vec3& _eye_direction;
    const float _horizon_rotation;
    const bool _flip_horizontal;
    const bool _flip_vertical;

    /**
     * Getter for the transformation matrix that negates the eye position and rotation.
     * \return a 4d transformation matrix.
     */
    glm::mat4 getUVtransformationMatrix() const;

    /**
     * Getter for the rotation matrix based on the angle from _horizon_rotation
     * Adjust rotation so texture coordinates horizon match real world horizon
     * \return a 3d homogeneous rotation matrix
     */
    glm::mat3 getHorizonBiasMatrix() const;

    /**
     * Getter for the mirror matrix
     * \return a 3d homogeneous mirror matrix
     */
    glm::mat3 getFlipMatrix() const;

    /**
     * Remap a set of uv coordinates inside a 1x1 square, keeping ratios intact.
     * \param uvs a vector of 2d coordinates bounded inside an arbitrary sized rectangle.
     */
    void uvUnitSquareRemap(std::vector<glm::vec2>& uvs) const;

    /**
     * Remap a value from between 2 values to between 2 other values, keeping its ratio.
     * \param value the value, which lies in range [from1, to1] to be remapped in range [from2, to2].
     * \return the remapped value
     */
    inline float remap(float value, float from1, float to1, float from2, float to2) const { return (value - from1) / (to1 - from1) * (to2 - from2) + from2; }
};

} // namespace calimiro

#endif // __CALIMIRO_TEXTURE_COORDINATES_GENERATOR__
