/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_MAP_XYZS__
#define __CALIMIRO_MAP_XYZS__

#include <filesystem>
#include <fstream>
#include <time.h> // used to generate a seed for the random number generator

#include "./calimiro/abstract_logger.h"
#include "./calimiro/calibration_point.h"
#include "./calimiro/utils.h"

namespace calimiro
{
class MapXYZs
{
  public:
    struct match
    {
        int position;
        int pixel_x;
        int pixel_y;
        double x;
        double y;
        double z;
        int prj_x;
        int prj_y;
        int prj_nb;
    };

    // Constructor
    MapXYZs(AbstractLogger* log, std::filesystem::path p = utils::getWorkspace());

    // Update the matches vector with the 2D coordinates in the "projector space" and the projector ID
    void pixelToProj(double scale = 0.125);

    // Downsample the matches vector per projector ID
    // Returns a vector of matches
    std::map<int, std::vector<CalibrationPoint>> sampling(unsigned int nb_pts = 30);

  private:
    AbstractLogger* _log;
    std::filesystem::path _workspace;
    std::vector<match> _matches;
};

}; // namespace calimiro

#endif // __CALIMIRO_MAP_XYZS__
