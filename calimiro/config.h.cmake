#ifndef __CALIMIRO_CONFIG__
#define __CALIMIRO_CONFIG__

/*Define the path to Meshlabserver executable */
#cmakedefine MESHLABSERVER "@MESHLABSERVER@"

/*Define the CMAKE_INSTALL_PREFIX */
#cmakedefine CMAKE_INSTALL_PREFIX "@CMAKE_INSTALL_PREFIX@"

#endif // __CALIMIRO_CONFIG__
