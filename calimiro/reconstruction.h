/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CALIMIRO_RECONSTRUCTION__
#define __CALIMIRO_RECONSTRUCTION__

#include <filesystem>

#include <opencv2/opencv.hpp>

#include "./calimiro/abstract_logger.h"
#include "./calimiro/config.h"
#include "./calimiro/constants.h"
#include "./calimiro/utils.h"

namespace calimiro
{

class Reconstruction
{
  public:
    enum CameraModel
    {
        PINHOLE_CAMERA,
        PINHOLE_CAMERA_RADIAL1,
        PINHOLE_CAMERA_RADIAL3,
        PINHOLE_CAMERA_BROWN,
        PINHOLE_CAMERA_FISHEYE,
        CAMERA_SPHERICAL
    };

  public:
    explicit Reconstruction(
        AbstractLogger* log, std::filesystem::path p = utils::getWorkspace(), CameraModel camera_model = CameraModel::PINHOLE_CAMERA_RADIAL3, bool write_details = false);
    ~Reconstruction();

    /**
     * Creation of the sfm_data.json describing the image dataset. For more details, see https://openmvg.readthedocs.io/en/latest/software/SfM/SfMInit_ImageListing/
     * \param focal_pixels: focal length in pixels
     */
    void sfmInitImageListing(double focal_pixels = -1.0);

    /**
     * Compute features and descriptors for each view
     */
    void computeFeatures();

    /**
     * Compute matches between the views
     */
    void computeMatches();

    /**
     * Incremental SfM
     */
    void incrementalSfM();

    /**
     * Structure from poses: compute and triangulate features using the cameras intrinsics and poses
     */
    void computeStructureFromKnownPoses();

    /**
     * Convert SfM format
     */
    void convertSfMStructure();

  private:
    class Impl;
    std::unique_ptr<Impl> _impl;
};
}; // namespace calimiro

#endif // __CALIMIRO_RECONSTRUCTION__
