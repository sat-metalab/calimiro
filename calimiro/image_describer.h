/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CALIMIRO_IMAGE_DESCRIBER__
#define __CALIMIRO_IMAGE_DESCRIBER__

#include <filesystem>

#include <cereal/archives/json.hpp>
#include <cereal/cereal.hpp>
#include <cereal/types/polymorphic.hpp>
#include <openMVG/features/image_describer.hpp>
#include <opencv2/opencv.hpp>

#include "./region_factory.h"
#include "./utils.h"

using namespace openMVG::features;

class STRUCTURED_LIGHT_Image_describer : public Image_describer
{
  public:
    template <class Archive>
    inline void serialize(Archive&){};
    STRUCTURED_LIGHT_Image_describer()
        : Image_describer(){};

    struct map_xy
    {
        int proj_x;
        int proj_y;
        int cam_x;
        int cam_y;
        int proj_id;
    };

    bool Set_configuration_preset(EDESCRIBER_PRESET) override { return false; };
    std::unique_ptr<Regions> Allocate() const override { return std::unique_ptr<STRUCTURED_LIGHT_Regions>(new STRUCTURED_LIGHT_Regions); };
    std::unique_ptr<Regions> Describe(const openMVG::image::Image<unsigned char>&, const openMVG::image::Image<unsigned char>* = nullptr) override { return {}; };
    std::unique_ptr<Regions> Describe(const std::filesystem::path& matrix_path) { return Describe_STRUCTURED_LIGHT(matrix_path); };

    std::unique_ptr<STRUCTURED_LIGHT_Regions> Describe_STRUCTURED_LIGHT(const std::filesystem::path& matrix_path)
    {
        auto regions = std::unique_ptr<STRUCTURED_LIGHT_Regions>(new STRUCTURED_LIGHT_Regions);
        cv::FileStorage fs;
        // check that the provided path is valid
        if (!fs.open(matrix_path.string(), cv::FileStorage::READ))
            return regions;

        cv::Mat3i matrix;
        fs[matrix_path.stem().string()] >> matrix;
        int cols = fs[matrix_path.stem().string()]["cols"];
        int rows = fs[matrix_path.stem().string()]["rows"];
        std::vector<map_xy> coords;
        coords.reserve(cols * rows);
        int constant = 1000;
        // Loop over the matrix to collect the correspondence between projected image and captured image
        for (int i = 0; i < rows; i += 1)
        {
            for (int j = 0; j < cols; j += 1)
            {
                // for each pixel, add the structure to the vector of correspondence if the value of the pixel is not zero
                if (matrix(i, j)[0] != 0 && matrix(i, j)[1] != 0)
                    coords.push_back({matrix(i, j)[0] * constant, matrix(i, j)[1] * constant, j, i, matrix(i, j)[2] * constant});
            }
        }
        fs.release();

        // A projector pixel corresponds to multiple camera pixels. We want a one-on-one correspondence. Therefore, for each projector pixel we find its cluster of corresponding
        // camera pixels and compute their means.
        Descriptor<float, 3> descriptor;
        std::map<int, std::vector<map_xy>> coords_by_id;
        std::map<int, std::vector<map_xy>> coords_by_x;
        std::map<int, std::vector<map_xy>> coords_by_y;

        std::sort(coords.begin(), coords.end(), [](const map_xy& lhs, const map_xy& rhs) { return lhs.proj_id < rhs.proj_id; });
        std::for_each(coords.begin(), coords.end(), calimiro::utils::inserter<int, map_xy>(coords_by_id, [](map_xy a) { return a.proj_id; }));
        for (auto& proj : coords_by_id)
        {
            std::sort(proj.second.begin(), proj.second.end(), [](const map_xy& lhs, const map_xy& rhs) { return lhs.proj_x < rhs.proj_x; });
            std::for_each(proj.second.begin(), proj.second.end(), calimiro::utils::inserter<int, map_xy>(coords_by_x, [](map_xy a) { return a.proj_x; }));

            for (auto& xs : coords_by_x)
            {
                std::sort(xs.second.begin(), xs.second.end(), [](const map_xy& lhs, const map_xy& rhs) { return lhs.proj_y < rhs.proj_y; });
                std::for_each(xs.second.begin(), xs.second.end(), calimiro::utils::inserter<int, map_xy>(coords_by_y, [](map_xy a) { return a.proj_y; }));

                for (auto& ys : coords_by_y)
                {
                    float mean_x = 0;
                    float mean_y = 0;
                    int num_elt = 0;
                    for (auto& camera : ys.second)
                    {
                        mean_x = mean_x + camera.cam_x;
                        mean_y = mean_y + camera.cam_y;
                        num_elt = num_elt + 1;
                    }
                    mean_x = mean_x / num_elt;
                    mean_y = mean_y / num_elt;

                    descriptor[0] = static_cast<float>(xs.first);
                    descriptor[1] = static_cast<float>(ys.first);
                    descriptor[2] = static_cast<float>(proj.first);
                    regions->Features().push_back(PointFeature(static_cast<int>(mean_x), static_cast<int>(mean_y))); // keypoint = (x, y)
                    regions->Descriptors().push_back(descriptor);
                }
                coords_by_y.clear();
            }
            coords_by_x.clear();
        }
        return regions;
    };
};
CEREAL_REGISTER_TYPE_WITH_NAME(STRUCTURED_LIGHT_Image_describer, "STRUCTURED_LIGHT_Image_describer");
CEREAL_REGISTER_POLYMORPHIC_RELATION(Image_describer, STRUCTURED_LIGHT_Image_describer);

#endif // __CALIMIRO_IMAGE_DESCRIBER__
