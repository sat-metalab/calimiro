/*
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CALIMIRO_OPTIMIZER__
#define __CALIMIRO_OPTIMIZER__

#include <gsl/gsl_multifit_nlinear.h>

#include "./abstract_logger.h"
#include "./calibration_point.h"
#include "./camera.h"
#include "./utils.h"

namespace calimiro
{

class Optimizer
{
  public:
    // constructor
    Optimizer(AbstractLogger* log, const std::shared_ptr<Camera>& camera)
        : _log(log)
        , _camera_ptr(camera){};
    virtual std::vector<double> optimize(const size_t nb_params, const std::vector<CalibrationPoint>& xs) = 0;

  protected:
    AbstractLogger* _log;
    static double penalty(const double);
    std::shared_ptr<Camera> _camera_ptr;
};

class Levenberg_Marquardt : public Optimizer
{
  public:
    Levenberg_Marquardt(AbstractLogger* log, const std::shared_ptr<Camera>& camera)
        : Optimizer(log, camera){};
    std::vector<double> optimize(const size_t nb_params, const std::vector<CalibrationPoint>& xs) override;
    static double error(const CalibrationPoint& pt, const glm::dvec2& pixel);

  private:
    static int costFunct(const gsl_vector* x, void* params, gsl_vector* f);
    static void callback(const size_t iter, void* params, const gsl_multifit_nlinear_workspace* w);
};

class Nelder_Mead : public Optimizer
{
  public:
    Nelder_Mead(AbstractLogger* log, const std::shared_ptr<Camera>& camera)
        : Optimizer(log, camera){};
    std::vector<double> optimize(const size_t nb_params, const std::vector<CalibrationPoint>& xs) override;
    static double error(const CalibrationPoint& pt, const glm::dvec2& pixel, const double z = std::numeric_limits<double>::quiet_NaN());

  private:
    static double costFunc(const gsl_vector* v, void* params);
};
} // namespace calimiro
#endif // __CALIMIRO_OPTIMIZER__
