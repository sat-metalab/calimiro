#
# This file is part of Calimiro.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Calimiro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
#
include(ExternalProject)

# build pmp-lib
ExternalProject_Add(pmp-external
   SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/pmp-library
   CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_PREFIX_PATH}
              -DCMAKE_CXX_FLAGS:STRING=-fPIC
              -DCMAKE_BUILD_TYPE=Release
              -DPMP_BUILD_APPS=OFF
              -DPMP_BUILD_EXAMPLES=OFF
              -DPMP_BUILD_VIS=OFF
)

install(DIRECTORY ${CMAKE_PREFIX_PATH}/include/pmp DESTINATION "include")
install(FILES
    ${CMAKE_PREFIX_PATH}/lib/libpmp.so
    ${CMAKE_PREFIX_PATH}/lib/libpmp.so.1.2.0
    DESTINATION "lib")

# set its properties
add_library(pmp-lib SHARED IMPORTED GLOBAL)
add_dependencies(pmp-lib pmp-external)
set(pmp_LIBRARIES ${CMAKE_PREFIX_PATH}/lib/libpmp.so)
set_target_properties(pmp-lib PROPERTIES IMPORTED_LOCATION ${pmp_LIBRARIES})

# build openMVG
ExternalProject_Add(openMVG-external
   SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/openMVG
   CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_PREFIX_PATH}
              -DCMAKE_CXX_FLAGS:STRING=-fPIC
              -DCMAKE_BUILD_TYPE=Release
              -DOpenMVG_BUILD_SOFTWARES=OFF
              -DOpenMVG_BUILD_EXAMPLES=OFF
              -DOpenMVG_BUILD_TESTS=OFF
    SOURCE_SUBDIR src/
)

# set it's properties
add_library(openMVG SHARED IMPORTED GLOBAL)
add_dependencies(openMVG openMVG-external)
set(openMVG_LIBRARIES ${CMAKE_PREFIX_PATH}/lib/)
set_target_properties(openMVG PROPERTIES IMPORTED_LOCATION ${openMVG_LIBRARIES})

set(openMVG_INCLUDES ${CMAKE_PREFIX_PATH}/include
#                      ${CMAKE_PREFIX_PATH}/include/openMVG_dependencies
)
include_directories(${openMVG_INCLUDES})
set_target_properties(openMVG PROPERTIES INCLUDE_DIRECTORIES openMVG_INCLUDES)

install(FILES
        ./third_parties/share/openMVG/sensor_width_camera_database.txt
        DESTINATION "share/calimiro/"
)
