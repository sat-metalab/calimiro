/*
 * This file is part of Calimiro.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Calimiro is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Calimiro.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // Get asserts in release mode

#include <cassert>
#include <cmath>
#include <iostream>
#include <limits>

#include "./calimiro/bqueue.h"

using namespace calimiro;

int main()
{
    Bqueue<int> bq = Bqueue<int>(3);
    // Test the capacity functionality
    assert((bq.capacity() == 3) == true);

    // Test the empty functionality
    assert(bq.empty() == true);

    // Test the size functionality
    assert((bq.size() == 0) == true);
    // insert an element with a value of 0 and priority of 0.0
    int zero = 0;
    bq.insert(zero, 0.0);
    assert((bq.size() == 1) == true);

    // Test the peek functionality
    int two = 2;
    bq.insert(two, 1.0);
    int three = 3;
    bq.insert(three, 0.6);
    // at this point, the bqueue contains 3 elements with priority of 0.0, 1.0 and 0.6.
    // Peek() and Pop() will return the max of the priority
    float max_priority = 1.0;
    float epsilon = std::numeric_limits<float>::epsilon();
    assert((fabs(bq.peek().priority - max_priority) < epsilon) == true);
    assert((*bq.peek().value == 2) == true);

    // Test the pop functionality
    assert((*bq.pop().value == 2) == true);

    // Verify that when inserting in a full queue, the element is only added if its priority is lower than the max
    bq.insert(two, 1.0);
    int one = 1;
    bq.insert(one, 0.8);
    // the queue still contains 3 elements. Element with a value 2 and priority 1.0 should have been popped to insert element with value 1 and priority 0.8
    assert((fabs(bq.peek().priority - 0.8) < epsilon) == true);
    assert((*bq.pop().value == 1) == true);
    assert((*bq.pop().value == 3) == true);
    assert((*bq.pop().value == 0) == true);
    assert(bq.empty() == true);

    return 0;
}
